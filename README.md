# CONSIGNAS

```Note:
Columna 2 y 4: Realicen un fork del repositorio.
Columna 1 y 3: Realicen un clone del repositorio forkeado por su compañero de mesa.
```

### Usuario columna par
1. Reemplazar los carácteres erróneos en el archivo "clase-1/modificar.txt".
    1. Hacer un add del archivo y luego un commit.
    1. En el mensaje del commit dar una frase descriptiva de lo realizado. Ej: "Corregidos los carácteres erróneos"
    1. Hacer un add, commit y push.

### Usuario columna impar
1. Hacer un fetch, y en caso que haya nuevos cambios, relizar un pull para descargarlos. 
    1. Dentro de clase-1, crear un archivo llamado "tmp.txt" y escribir algo. Ej "Estoy haciendo el curso de GIT".
    1. Hacer un add, commit y push. 

### Usuario columna par
1. Hacer un fetch y luego un pull
    1. Borrar el archivo "clase-1/tmp.txt"
    2. Hacer un add, commit, push.

### Usuario columna impar
1. Hacer un pull.
    1. Renombrar el archivo "modificar.txt" por "corregido.txt" y mover el archivo dentro de un nuevo directorio llamado "tarea".
    1. Hacer un add, commit y push. 
